return {
  'Exafunction/codeium.vim',
  config = function ()
    -- Change '<C-g>' here to any keycode you like.
    vim.g.codeium_disable_bindings = 1
    vim.keymap.set('i', '<C-g>', function () return vim.fn['codeium#Accept']() end, { expr = true, silent = true })
    vim.keymap.set('i', '<c-;>', function() return vim.fn['codeium#CycleCompletions'](1) end, { expr = true, silent = true })
    vim.keymap.set('i', '<c-,>', function() return vim.fn['codeium#CycleCompletions'](-1) end, { expr = true, silent = true })
    vim.keymap.set('i', '<c-x>', function() return vim.fn['codeium#Clear']() end, { expr = true, silent = true })
  end
}
--



-- return {
--   {
--     'huggingface/llm.nvim',
--     opts = {
--       model = "Meta-Llama-3-8B-Instruct-imatrix", -- the model ID, behavior depends on backend
--       backend = "openai",                         -- backend ID, "huggingface" | "ollama" | "openai" | "tgi"
--       url = "http://localhost:1234/v1/chat/completions",
--       request_body = {
--         messages = {
--           {
--             role = "user",
--             content = "You are a helpful, smart, kind, and efficient AI assistant. You always fulfill the user's requests to the best of your ability."
--           }
--         }
--       },
--       lsp = {
--         bin_path = vim.api.nvim_call_function("stdpath", { "data" }) .. "\\mason\\packages\\llm-ls\\llm-ls.exe",
--       },
--     }
--   },
-- }
