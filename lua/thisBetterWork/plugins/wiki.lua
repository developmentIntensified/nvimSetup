local home = vim.fn.expand("$HOME")

return {
    {
        'dimfeld/section-wordcount.nvim',
        config = function()
            require('section-wordcount').setup()
            local markdownGroup = vim.api.nvim_create_augroup('markdown', {})

            vim.api.nvim_create_autocmd({ 'BufRead', 'BufNewFile' }, {
                group = markdownGroup,
                pattern = '*.md',
                callback = function()
                    vim.opt_local.spell = true
                    vim.opt_local.linebreak = true
                    vim.opt_local.formatoptions:append 't'
                end,
            })

            vim.api.nvim_create_autocmd('FileType', {
                group = markdownGroup,
                pattern = 'markdown',
                callback = function()
                    require('section-wordcount').wordcounter({})
                end
            })

            -- local norgGroup = vim.api.nvim_create_augroup('markdown', {})
            --
            -- vim.api.nvim_create_autocmd({ 'BufRead', 'BufNewFile' }, {
            --     group = norgGroup,
            --     pattern = '*.org',
            --     callback = function()
            --         vim.opt_local.spell = true
            --         vim.opt_local.linebreak = true
            --         vim.opt_local.formatoptions:append 't'
            --     end,
            -- })
            --
            -- vim.api.nvim_create_autocmd('FileType', {
            --     group = norgGroup,
            --     pattern = 'org',
            --     callback = function()
            --         require('section-wordcount').wordcounter({})
            --     end
            -- })
        end
    }, {
    'serenevoid/kiwi.nvim',
    dependencies = {
        "nvim-lua/plenary.nvim"
    },
    opts = {
        {
            name = "school",
            path = home .. "/wiki/school"
        },
        {
            name = "filterlink",
            path = home .. "/wiki/filterlink"
        },
        {
            name = "work",
            path = home .. "/wiki/work"
        },
        {
            name = "personal",
            path = home .. "/wiki/personal"
        }
    },
    keys = {
        { "<leader>ww", ":lua require(\"kiwi\").open_wiki_index()<cr>", desc = "Open Wiki index" },
        { "<leader>t",  ":lua require(\"kiwi\").todo.toggle()<cr>",     desc = "Toggle Markdown Task" }
    }
},
    {
        "folke/todo-comments.nvim",
        dependencies = { "nvim-lua/plenary.nvim" },
        config = function()
            require("todo-comments").setup({
                keywords = {
                    FIXED = {
                        color = "info",
                    }
                },
                highlight = {
                    comments_only = false
                }
            })

            vim.keymap.set("n", "]t", function()
                require("todo-comments").jump_next()
            end, { desc = "Next todo comment" })

            vim.keymap.set("n", "[t", function()
                require("todo-comments").jump_prev()
            end, { desc = "Previous todo comment" })

            vim.keymap.set("n", "<leader>st", "<cmd>TodoTelescope<cr>", { desc = "Todo comments" })

            -- You can also specify a list of valid jump keywords
            -- vim.keymap.set("n", "]t", function()
            --   require("todo-comments").jump_next({keywords = { "ERROR", "WARNING" }})
            -- end, { desc = "Next error/warning todo comment" })
        end
    },
    {
        "tadmccorkle/markdown.nvim",
        ft = "markdown", -- or 'event = "VeryLazy"'
        opts = {
            -- configuration here or empty for defaults
        },
    },
    {
        'Nedra1998/nvim-journal',
        config = {
            default = nil,
            journals = {
                ["personal"] = {
                    path = home .. "/wiki/personal/journal",
                    frequency = "daily",
                    filename = "%Y-%m-%d.md",
                    template = {
                        create = "# %a %b %d %T %Y\n\n",
                        update = "## %H:%M\n\n",
                    },
                    index = {
                        filename = "README.md",
                        header = "# Journal Index\n\n",
                        sort = "descending",
                        entry = "%Y-%m-%d",
                        sections = {
                            "## %Y", "### %B, %Y"
                        },
                    },
                },

                ["work"] = {
                    path = home .. "/wiki/work/journal",
                    frequency = "daily",
                    filename = "%Y-%m-%d.md",
                    template = {
                        create = "# %a %b %d %T %Y\n\n",
                        update = "## %H:%M\n\n",
                    },
                    index = {
                        filename = "README.md",
                        header = "# Journal Index\n\n",
                        sort = "descending",
                        entry = "%Y-%m-%d",
                        sections = {
                            "## %Y", "### %B, %Y"
                        },
                    },
                },
            }
        }
    },

    "itchyny/calendar.vim"
}
