return { {
   "andrewferrier/debugprint.nvim",
   opts = {
   },
   -- Dependency only needed for NeoVim 0.8
   dependencies = {
      "nvim-treesitter/nvim-treesitter"
   },
   -- Remove the following line to use development versions,
   -- not just the formal releases
   version = "*"
},
   -- 'mfussenegger/nvim-dap',
   -- { "mxsdev/nvim-dap-vscode-js", requires = {"mfussenegger/nvim-dap"} }
   {
      "folke/trouble.nvim",
      dependencies = { "nvim-tree/nvim-web-devicons" },
      opts = {
         -- your configuration comes here
         -- or leave it empty to use the default settings
         -- refer to the configuration section below
      },
   }
}
